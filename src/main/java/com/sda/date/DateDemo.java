package com.sda.date;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateDemo {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        LocalTime today = LocalTime.now();
        System.out.println(now);
        System.out.println(today);
        LocalDateTime newYear =
                LocalDateTime.of(LocalDate.of(2020,1,1),
                        LocalTime.of(0,0,0));
        System.out.println(newYear);
        LocalDateTime justNow= LocalDateTime.now();
        boolean isAfter = justNow.isAfter(newYear);
        LocalDateTime yesterday = justNow.minusDays(1);
        System.out.println(yesterday);
        ZonedDateTime timeZoned= ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        System.out.println(timeZoned);

        Duration duration = Duration.ofDays(1);
        Period period = Period.ofYears(3);
        LocalDateTime x= yesterday.plus(duration);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String data = format.format(yesterday);
        System.out.println(data);
        // throws error because now is LocalDate and formatter have hours
//        String otherData = now.format(format);
//        System.out.println(otherData);
    }
}
