package com.sda.immutable;

import java.util.Collections;
import java.util.List;

public class Grup {
    private List<Student> listaStudenti;

    public Grup(List<Student> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public List<Student> getListaStudenti() {
        return Collections.unmodifiableList(listaStudenti);
    }
}
