package com.sda.imports;

//import all classes from this package
import java.util.*;
// import specific class
import java.util.Scanner;

//import all static methods from Math class
import static java.lang.Math.*;
//import a specific static method
import static java.util.Collections.reverse;

// citim de la tastatura numere pana introducem 0
// afisam minimul si maximul si lista in ordine inversa pentru a putea urmarii cele 4 moduri de import
public class ImportDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();
        int number;
        do {
            number = scanner.nextInt();
            numbers.add(number);
        } while (number!=0);
        int min=numbers.get(0);
        int max=numbers.get(0);
        for(int i=1;i<numbers.size();i++){
            min= min(min,numbers.get(i));
            max= max(max,numbers.get(i));
        }
        reverse(numbers);
        System.out.println("Min "+min);
        System.out.println("Max "+max);
        System.out.println("Reversed list");
        System.out.println(numbers);
    }
}
