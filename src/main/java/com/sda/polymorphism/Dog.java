package com.sda.polymorphism;

public class Dog extends Animal {

    @Override
    public void makeSound(){
        System.out.println("Ham-ham!");
    }

    public void eat(){
        System.out.println("Eat bones");
    }
}
