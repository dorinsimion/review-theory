package com.sda.polymorphism;

public class AnimalDemo {
    public static void main(String[] args) {
        Animal animal = new Dog();
        animal.makeSound();
        //don't cast without checking first using instanceof
        ((Dog) animal).eat();

        Dog dog = new Dog();
        dog.eat();

        Animal cat = new Cat();
        if(cat instanceof Dog) {
            ((Dog) cat).eat();
        }
    }
}
