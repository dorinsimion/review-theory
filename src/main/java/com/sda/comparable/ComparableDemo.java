package com.sda.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparableDemo {
    public static void main(String[] args) {
        Student s1= new Student("Ion",6);
        Student s2= new Student("George",7);
        if(s1.compareTo(s2)<0){
            System.out.println(s1 +" are nota mai mica");
        }

        List<Student> list = new ArrayList<>();
        list.add(s1);
        list.add(s2);
        Collections.sort(list);
        System.out.println(list);
    }
}
