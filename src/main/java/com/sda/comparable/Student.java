package com.sda.comparable;

public class Student implements Comparable<Student> {
    private String name;
    private int nota;

    public Student(String name, int nota) {
        this.name = name;
        this.nota = nota;
    }

    @Override
    public int compareTo(Student o) {
//        return name.compareTo(o.name);
        return nota-o.nota;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", nota=" + nota +
                '}';
    }
}
