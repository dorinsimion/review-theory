package com.sda.interfaces;

public class BirdDemo {
    public static void main(String[] args) {
        Bird bird = new Bird();
        bird.fly();
        bird.fly(4);
    }
}
