package com.sda.interfaces;

import java.io.Serializable;

public interface Fly {

    void fly();

    default void fly(int wings){
        System.out.println("Fly with "+ wings);
    }
}
