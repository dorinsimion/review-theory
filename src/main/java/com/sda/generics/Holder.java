package com.sda.generics;

import com.sda.polymorphism.Animal;

public class Holder<T extends Animal> {
    private T var;

    public Holder(T var) {
        this.var = var;
    }

    public T getVar() {
        return var;
    }
}
