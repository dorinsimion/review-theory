package com.sda.generics;

import com.sda.polymorphism.Animal;
import com.sda.polymorphism.Dog;

import java.util.ArrayList;
import java.util.List;

public class Generics {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("Student1");
        list.add("Student2");
        list.add("Student3");
        list.add(new Object());
//        ((String)list.get(3)).charAt(1);

        List<String> studenti = new ArrayList<>();
        studenti.add("Student1");
        studenti.get(0).charAt(1);

        Holder<Animal> holder1 = new Holder<>(new Animal());
        holder1.getVar().makeSound();

        Holder<Dog> holder2 = new Holder<>(new Dog());
        holder2.getVar().eat();
    }
}
